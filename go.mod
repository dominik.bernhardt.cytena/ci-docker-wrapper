module ci-docker-wrapper

go 1.17

require github.com/pieterclaerhout/go-log v1.14.0

require (
	github.com/fatih/color v1.9.0 // indirect
	github.com/golang/protobuf v1.2.0 // indirect
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.1 // indirect
	github.com/hashicorp/go-retryablehttp v0.6.8 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.11 // indirect
	github.com/pieterclaerhout/go-formatter v1.0.4 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rotisserie/eris v0.4.0 // indirect
	github.com/sanity-io/litter v1.2.0 // indirect
	github.com/tidwall/gjson v1.3.2 // indirect
	github.com/tidwall/match v1.0.1 // indirect
	github.com/tidwall/pretty v1.0.0 // indirect
	github.com/xanzy/go-gitlab v0.51.1 // indirect
	golang.org/x/net v0.0.0-20201021035429-f5854403a974 // indirect
	golang.org/x/oauth2 v0.0.0-20181106182150-f42d05182288 // indirect
	golang.org/x/sys v0.0.0-20200930185726-fdedc70b468f // indirect
	golang.org/x/text v0.3.3 // indirect
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	google.golang.org/appengine v1.3.0 // indirect
)
