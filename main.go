package main

/*
	A simple wrapper around docker run used for gitlab-ci environment on windows.
	The wrapper forwards all GITLAB_* and CI_ variables, mounts the current dir and executes the provided
	command in the container. If a signal (sigint or sigterm) is recieved the container ist stopped.
*/

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"regexp"
	"strings"
	"syscall"
	"time"

	"github.com/pieterclaerhout/go-log"
	"github.com/xanzy/go-gitlab"
)

func get_container_name() string {
	container_name := "ci-container-" + fmt.Sprint(os.Getpid())
	return container_name
}

func run_docker(docker_img string, command_args []string, docker_return chan int) {
	current_dir, err := os.Getwd()
	log.CheckError(err)
	// compose docker arguments
	docker_args := []string{"run", "--rm", "--name", get_container_name(), "-v", current_dir + "\\:c:/src", "-w", "c:/src", "--env-file", "gitlab_ci_env_list.txt"}
	if _, err := os.Stat("env_variables_for_ci.txt"); err == nil {
		docker_args = append(docker_args, "--env-file", "env_variables_for_ci.txt")
	}
	docker_args = append(docker_args, docker_img)
	docker_args = append(docker_args, command_args...)
	log.Info(strings.Join(docker_args, " "))
	cmd := exec.Command("docker", docker_args...)
	// Get a pipe to read from standard out
	r, _ := cmd.StdoutPipe()
	// Use the same pipe for standard error
	cmd.Stderr = cmd.Stdout
	// Make a new channel which will be used to ensure we get all output
	done := make(chan bool)
	// Create a scanner which scans r in a line-by-line fashion
	scanner := bufio.NewScanner(r)
	// Use the scanner to scan the output line by line and log it
	// It's running in a goroutine so that it doesn't block
	go func() {
		// Read line by line and process it
		for scanner.Scan() {
			line := scanner.Text()
			log.Info(line)
		}
		// We're all done, unblock the channel
		done <- true
	}()
	// Start the command and check for errors
	err = cmd.Start()
	log.CheckError(err)
	// Wait for all output to be processed
	<-done
	// Wait for the command to finish
	err = cmd.Wait()
	log.CheckError(err)
	docker_return <- 0
}

func stop_docker() {
	log.Warn("Stopping docker container")
	cmd := exec.Command("docker", "stop", get_container_name())
	cmd_output, err := cmd.Output()
	log.Info(cmd_output)
	log.CheckError(err)
}

func monitor_gitlab_ci_job() {
	job_token := os.Getenv("CI_JOB_TOKEN")
	api_url := os.Getenv("CI_API_V4_URL")
	if job_token == "" {
		log.Warn("CI_JOB_TOKEN not specified. Won't monitor CI job for cancellation")
		return
	}
	log.Info("Start monitoring CI job")
	for {
		resp, err := http.Get(api_url + "/job?job_token=" + job_token)
		log.CheckError(err)
		body, err := ioutil.ReadAll(resp.Body)
		log.CheckError(err)
		job_data := gitlab.Job{}
		err = json.Unmarshal([]byte(body), &job_data)
		log.CheckError(err)
		if job_data.Status != "running" {
			log.Warn("CI Job status is not running: " + job_data.Status)
			stop_docker()
		}
		time.Sleep(time.Second * 10)
	}
}

func create_ci_env_var_file() {
	log.Info("Scanning for CI environment variables")
	r, err := regexp.Compile("^(CI.*|GITLAB.*)")
	log.CheckError(err)
	f, err := os.Create("gitlab_ci_env_list.txt")
	log.CheckError(err)
	defer f.Close()
	for _, e := range os.Environ() {
		pair := strings.SplitN(e, "=", 2)
		if r.MatchString(pair[0]) {
			f.WriteString(pair[0] + "\n")
		}
	}
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Usage: ci-docker-wrapper <image> <command>")
	}
	log.Info("CI Docker Wrapper")
	log.PrintTimestamp = true
	create_ci_env_var_file()
	docker_return := make(chan int)
	signal_channel := make(chan os.Signal, 1)
	signal.Notify(signal_channel, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		sig := <-signal_channel
		log.Info("Recieved signal: " + sig.String())
		stop_docker()
	}()
	go run_docker(os.Args[1], os.Args[2:], docker_return)
	go monitor_gitlab_ci_job()
	log.Info("Waiting for docker to return")
	return_value := <-docker_return
	log.Info("docker returned: " + fmt.Sprint(return_value))
	os.Exit(return_value)
}
