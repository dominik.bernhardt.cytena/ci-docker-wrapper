# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.4] - 2021-11-10
### Changed
- If not running on CI do not try to monitor job

## [0.1.3] - 2021-11-02
### Added
- Added gitlab job monitoring

## [0.1.2] - 2021-11-01
### Added
- Initial version

[Unreleased]: https://gitlab.com/bico-group/cytena/ci-infrastructure/ci-docker-wrapper/compare/v0.1.4...master
[0.1.4]: https://gitlab.com/bico-group/cytena/ci-infrastructure/ci-docker-wrapper/compare/v0.1.3...v0.1.4
[0.1.3]: https://gitlab.com/bico-group/cytena/ci-infrastructure/ci-docker-wrapper/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/bico-group/cytena/ci-infrastructure/ci-docker-wrapper/tree/v0.1.3